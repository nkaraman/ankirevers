# Description #
It is a tool for swapping question and answer inside of text file. 
This is required whenever you need to work with Anki flashcards and trying to learn a word translation
in both sides.

What is Anki?
https://apps.ankiweb.net/
Friendly, intelligent flash cards.
Remembering things just became much easier.

### How do I get set up? ###

You would need installed:
* java 8
* maven

mvn clean package

Than you could run exe on Windows PC or jar's for any others. 