package org.mkaraman.anki.revers;

import org.apache.commons.lang3.tuple.Pair;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertThat;

class AnkiFileProcessorTest {

    public static final String RESOURCES = "src/test/resources/";
    private AnkiFileProcessor processor = new AnkiFileProcessor();

    @DisplayName("Test that separator read properly as X and data as well")
    @Test
    void readFromFileSeparator() throws IOException {
        Pair<String, String> result = processor.readFromFile(new File(RESOURCES + "anki_small_x_separator.txt"));

        String data = "Mitternacht ;00:00,\n" +
            "ein Freund von mir ; друг ( не девушка/парень/ НЕ по любви )\n";

        assertThat(result.getLeft(), Matchers.equalTo("X"));
        assertThat(result.getRight(), Matchers.equalTo(data));
    }

    @DisplayName("Test that default separator is picked properly")
    @Test
    void readFromFileDefaultSeparator() throws IOException {
        Pair<String, String> result = processor.readFromFile(new File(RESOURCES + "anki_small_no_separator.txt"));

        String data = "Mitternacht ;00:00,\n" +
            "ein Freund von mir ; друг ( не девушка/парень/ НЕ по любви )\n";

        assertThat(result.getLeft(), Matchers.equalTo(";"));
        assertThat(result.getRight(), Matchers.equalTo(data));
    }

    @DisplayName("Gets a new file name")
    @Test
    void getFileInTheSameFolder1() throws IOException {
        File file = processor.getFileInTheSameFolder(new File(RESOURCES + "anki_small.txt"));

        if(file.exists()) {
            assertThat(file.getName(), Matchers.equalTo("generated_anki_small.txt"));
            file.delete();
        } else {
            Assert.fail("File was not created");
        }
    }

    @DisplayName("Gets a new file name with number")
    @Test
    void getFileInTheSameFolder2() throws IOException {
        File generatedBeforeFile = new File(RESOURCES + "generated_anki_small.txt");
        generatedBeforeFile.createNewFile();

        File file = processor.getFileInTheSameFolder(new File(RESOURCES + "anki_small.txt"));

        generatedBeforeFile.delete();

        if(file.exists()) {
            assertThat(file.getName(), Matchers.equalTo("0_generated_anki_small.txt"));
            file.delete();
        } else {
            Assert.fail("File was not created");
        }
    }
}