package org.mkaraman.anki.revers;

import org.apache.commons.lang3.tuple.Pair;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertThat;

public class StructureParserTest {

    private StructureParser parser = new StructureParser();
    private AnkiFileProcessor fileProcessor = new AnkiFileProcessor();
    private Pair<String, String> separatorToData;

    @DisplayName("Parser ignore errors does not work")
    @Test
    void parseSkipErrorsTrue() throws IOException {
        separatorToData = fileProcessor.readFromFile(new File("src/test/resources/anki_small_with_error.txt"));

        List<Pair<String, String>> result = parser.parse(separatorToData, true);

        assertThat(result.size(), Matchers.equalTo(1));
        assertThat(result.get(0).getLeft(), Matchers.equalTo("ein Freund von mir "));
        assertThat(result.get(0).getRight(), Matchers.equalTo(" друг ( не девушка/парень/ НЕ по любви )"));
    }

    @DisplayName("Parser ignores errors in the file in all cases")
    @Test
    public void parseSkipErrorsFalse() throws IOException {
        separatorToData = fileProcessor.readFromFile(new File("src/test/resources/anki_small_with_error.txt"));
        Assertions.assertThrows(RuntimeException.class, () -> parser.parse(separatorToData, false));
    }

}