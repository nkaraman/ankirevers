package org.mkaraman.anki.revers;

import org.apache.commons.lang3.tuple.Pair;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class DataConverterTest {

    private DataConverter dataConverter = new DataConverter();

    @DisplayName("After conversion all values and mutation is inside, order is not followed")
    @Test
    void generateAnswerToQuestionAndRandomize() {
        List<Pair<String, String>> questionToAnswerList = Arrays.asList(
            Pair.of("a", "b"),
            Pair.of("c","d")
        );
        List<Pair<String, String>> result  = dataConverter.generateAnswerToQuestionAndRandomize(questionToAnswerList);
        Map<String, String> resultMap = convertToMap(result);

        //all values and mutations are generated
        assertThat(result, Matchers.notNullValue());
        assertThat(resultMap.get("a"), Matchers.equalTo("b"));
        assertThat(resultMap.get("b"), Matchers.equalTo("a"));
        assertThat(resultMap.get("c"), Matchers.equalTo("d"));
        assertThat(resultMap.get("d"), Matchers.equalTo("c"));

        // order check
        assertThat(result.get(0).getLeft(), Matchers.not("a"));
        assertThat(result.get(0).getRight(), Matchers.not("b"));

        assertThat(result.get(1).getLeft(), Matchers.not("c"));
        assertThat(result.get(1).getRight(), Matchers.not("d"));

        assertThat(result.get(2).getLeft(), Matchers.not("b"));
        assertThat(result.get(2).getRight(), Matchers.not("a"));

        assertThat(result.get(3).getLeft(), Matchers.not("d"));
        assertThat(result.get(3).getRight(), Matchers.not("c"));

    }

    Map<String, String> convertToMap(List<Pair<String, String>> questionToAnswerList) {
        return questionToAnswerList.stream().collect(Collectors.toMap(p -> p.getLeft() , p -> p.getRight()));
    }

}