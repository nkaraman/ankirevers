package org.mkaraman.anki.revers;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;

public class StructureParser {

    /**
     *
     * @param separatorToData
     * @return list of question to answer
     *
     * @throws RuntimeException in case of line is parsed incorrectly  and skipErrors == false
     */
    List<Pair<String, String>> parse(Pair<String, String> separatorToData, boolean skipErrors) {
        List<Pair<String, String>> result = new ArrayList<>();
        String[] questionToAnswerArray = separatorToData.getRight().split("\n");
        for (String questionToAnswerString : questionToAnswerArray) {
            if(StringUtils.isNotEmpty(questionToAnswerString)) {
                String[] questionToAnswer = questionToAnswerString.split(separatorToData.getLeft());
                if(questionToAnswer.length == 2) {
                    result.add(Pair.of(questionToAnswer[0], questionToAnswer[1]));
                } else {
                    if (!skipErrors) {
                        throw new RuntimeException("Somewhere in file separator " + separatorToData.getLeft() + " is used several times, "

                            + " in the line with: " + questionToAnswerString);
                    }
                }
            }
        }
        return result;
    }

    String parseToAFileFormat (String separator, List<Pair<String, String>> questionToAnswerList) {
        StringBuilder builder = new StringBuilder(separator).append("\n");
        questionToAnswerList.stream().forEach(p -> builder.append(p.getLeft()).append(separator).append(p.getRight()).append("\n"));

        return builder.toString();
    }
}
