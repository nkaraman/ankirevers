package org.mkaraman.anki.revers;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class AnkiFileProcessor {

    private static final String DEFAULT_SEPARATOR = ";";
    private static final String DEFAULT_FILE_PREFIX = "generated_";

    /**
     * @param file
     *
     * @return separator to data
     */
    public Pair<String, String> readFromFile(File file) throws IOException {
        StringBuffer buffer = new StringBuffer();
        String questionAnswerSeparator = DEFAULT_SEPARATOR;
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String firstLine = reader.readLine();
            if(StringUtils.isEmpty(firstLine)) {
                throw new IOException("Could not read file " + file.getAbsolutePath());
            }
            questionAnswerSeparator = firstLine.trim();
            if (questionAnswerSeparator.length() > 1) {
                buffer.append(questionAnswerSeparator);
                buffer.append("\n");
                questionAnswerSeparator = DEFAULT_SEPARATOR;
            }
            while (reader.ready()) {
                String line = reader.readLine();
                if (line.trim().length() > 0) {
                    buffer.append(line);
                    buffer.append("\n");
                }
            }
        }

        return Pair.of(questionAnswerSeparator, buffer.toString());
    }

    public File writeGeneratedFileWithData(File originalFile, String data) throws IOException {
        File generatedFile = getFileInTheSameFolder(originalFile);
        writeFile(generatedFile, data);
        return generatedFile;
    }

    File getFileInTheSameFolder(File original) throws IOException {
        String absolutFolderPath = original.getParentFile().getAbsolutePath();
        String fileName = DEFAULT_FILE_PREFIX + original.getName();
        File generatedFile = new File(absolutFolderPath + File.separator + fileName);
        if (generatedFile.createNewFile()) {
            return generatedFile;
        }

        for (int index = 0; index < 1_000_000; index++) {
            generatedFile = new File(absolutFolderPath + File.separator + index + "_" + fileName);
            if (generatedFile.createNewFile()) {
                return generatedFile;
            }
        }

        throw new IOException("Failed to create a file under path: " + generatedFile.getAbsolutePath());
    }

    void writeFile(File file, String data) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(data);
        }
    }
}
