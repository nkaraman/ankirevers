package org.mkaraman.anki.revers;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class Main extends JDialog {
    private final JFileChooser fileChooser = new JFileChooser();
    private final JButton pickAnkiFileButton = new JButton();
    private final JButton generateButton = new JButton();
    private final JTextArea errorMessageTextArea = new JTextArea();

    private final LogicService logicService = LogicService.getInstance();



	public Main() {
		setSize(200, 100);
		setTitle("Anki revers");

        pickAnkiFileButton.setText("Pick anki file");
        pickAnkiFileButton.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent event) {
                int returnValue = fileChooser.showOpenDialog(null);

                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    try {
                        File file = logicService.doSomeWork(selectedFile, true);
                        JOptionPane.showMessageDialog(null, "File was generated: " + file.getAbsolutePath());
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e.getMessage());
                        e.printStackTrace();
                    }
                }
            }
        });
        add(pickAnkiFileButton);

		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);

	}

	public static void main(final String[] args) {
		new Main();
	}

}
