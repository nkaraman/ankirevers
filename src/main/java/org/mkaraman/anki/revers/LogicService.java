package org.mkaraman.anki.revers;

import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class LogicService {
    private AnkiFileProcessor ankiFileProcessor;
    private DataConverter dataConverter;
    private StructureParser structureParser;

    public LogicService(
        AnkiFileProcessor ankiFileProcessor,
        DataConverter dataConverter,
        StructureParser structureParser
    ) {
        this.ankiFileProcessor = ankiFileProcessor;
        this.dataConverter = dataConverter;
        this.structureParser = structureParser;
    }

    public File doSomeWork(File originalFile, boolean skipErrors) throws Exception {
        Pair<String, String> separatorToData = ankiFileProcessor.readFromFile(originalFile);
        List<Pair<String, String>>  parsed = structureParser.parse(separatorToData, skipErrors);

        List<Pair<String, String>> originalWithMutation = dataConverter.generateAnswerToQuestionAndRandomize(parsed);
        String result = structureParser.parseToAFileFormat(separatorToData.getLeft(), originalWithMutation);

        return ankiFileProcessor.writeGeneratedFileWithData(originalFile, result);
    }

    public static final LogicService getInstance() {
        return new LogicService(new AnkiFileProcessor(), new DataConverter(), new StructureParser());
    }
}
