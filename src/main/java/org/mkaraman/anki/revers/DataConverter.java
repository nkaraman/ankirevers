package org.mkaraman.anki.revers;

import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class DataConverter {

    public List<Pair<String, String>> generateAnswerToQuestionAndRandomize(List<Pair<String, String>> questionToAnswerList) {

        List<Pair<String, String>> result = questionToAnswerList.stream().map(p -> Pair.of(
            p.getRight(),
            p.getLeft()
        )).collect(Collectors.toList());
        result = new ArrayList<>(result);
        result.addAll(questionToAnswerList);

        Collections.shuffle(result);

        return result;
    }
}
